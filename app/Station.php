<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Station extends Model
{
    protected $fillable = [
        'name', 'latitude', 'longitude', 'company_id'
    ];


    public function company(){
        return $this->belongsTo('App\Company', 'company_id');
    }
}
