<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{

    protected $fillable = [
        'name', 'parent_id'
    ];

    public function parent(){
        return $this->belongsTo('App\Company', 'parent_id');
    }

    public function parents(){
        return $this->parent()->with('parents');
    }

    public function children(){
        return $this->hasMany('App\Company', 'parent_id');
    }

    
    public function childs(){
        return $this->children()->with('childs');
    }

    public function stations(){
        return $this->hasMany('App\Station');
    }
}
