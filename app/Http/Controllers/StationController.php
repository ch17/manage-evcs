<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Station;
use App\Company;

class StationController extends Controller
{

    public function index(Request $request)
    {
        if($request->has('company_id')){

           $companies = Company::where('id', $request->company_id)->with('parents')->first()->toArray();

           $parent_companies = array();
           if($companies){
                $parent_companies[] = $companies['id'];
                $parent_companies = $this->list_companies($companies, $parent_companies);

                $stations = Station::whereIn('company_id', $parent_companies)->with('company')->paginate(20);

             
           }else{
                return response()->json(['message' => "Station not found!"], 201);
           }        
         

        }else{
            $stations = Station::with('company')->paginate(20);
        }

        
        return response()->json($stations, 200);
    }


    //CREATE STATION
    public function create(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required|max:255',     
            'latitude'      => 'required',     
            'longitude'     => 'required',  
            'company_id'    => 'required|integer',  
        ]);
        
        $stations = Station::create([
            'name'          => $request->name,
            'latitude'      => $request->latitude,
            'longitude'     => $request->longitude,
            'company_id'    => $request->company_id
        ]);

        return response()->json($stations, 200);
    }



    public function edit($id)
    {
        //
    }


    //UPDATE STATION    
    public function update($id, Request $request)
    {

        $this->validate($request, [
            'name'          => 'required|max:255',     
            'latitude'      => 'required',     
            'longitude'     => 'required',
            'company_id'     => 'required',  
        
        ]);
        
        
        if(!Station::find($id)) return response()->json(['message' => "Station not found!"], 201);
        
        $station = Station::find($id)->update($request->all());


        
        if($station){
            return response()->json(['data' => $station, 'message' => "Station updated successfully"], 200);
        }
        
        return response()->json(['message' => "Something wrong."], 200);
    }




    public function destroy($id)
    {
        if(!Station::find($id)) return response()->json(['message' => "Station not found!"] ,201);

        if(Station::find($id)->delete()){
            return response()->json(['message' => "Station deleted sucessfully."] ,200);
        }
        
        return response()->json(['message' => "Something wrong!"] ,201);
    }


    function list_companies(array $companies, &$data)
    {
   
        $parent_count = count($companies['parents']);         

            while ($parent_count):
            
                $parent_count = 0;
                $data[] = $companies['parents']['id'];
                $this->list_companies($companies['parents'], $data);
            
            endwhile;

       return $data;
     }   
  
}
