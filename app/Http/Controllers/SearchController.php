<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use App\Station;

class SearchController extends Controller
{

    public function filter(Request $request, Station $station)
    {
        $stations = $station->newQuery();

        if($request->has('distance')){
            $distance = $request->has('distance');
        }else{
            $distance = 25;
        }

        
        if ($request->has('latitude') && $request->has('longitude')) {

            $stations->select(\DB::raw('*, ( 6367 * acos( cos( radians('.$request->latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$request->longitude.') ) + sin( radians('.$request->latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
                    ->having('distance', '<', $distance)
                    ->orderBy('distance');            
        }

        $stations = $stations->with('company')->get();

        return response()->json($stations, 200);
    }

    
}
