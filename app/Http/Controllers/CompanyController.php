<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;

class CompanyController extends Controller
{

    public function index()
    {
        $companies = Company::with(['childs'])->where('parent_id', 0)->paginate(20);
        return response()->json($companies, 200);
    }

    public function show($id)
    {
        $stations = Company::with([
                        'stations',
                        'childs.stations'
                    ])->find($id)->paginate(20);

            
       return response()->json($stations, 200);
    }

    public function create(Request $request)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update($id, Request $request)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
