<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\User;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, \Closure $next)
    {
        $token = $request->get('token');
        
        if(!$token) {  
                       
            return response()->json([
                'success' => false,
                'errorMessage' => 'Token not provided.'
            ], 201);
        }

        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {

            return response()->json([
                'success' => false,
                'errorMessage' => 'Provided token is expired.'
            ], 201);

        } catch(Exception $e) {

            return response()->json([
                'success' => false,
                'errorMessage' => 'An error while decoding token.'
            ], 201);

        }
        $user = User::find($credentials->sub);
        
        $request->auth = $user;
        return $next($request);
    }
}