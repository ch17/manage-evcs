<?php
use App\Libraries\Helpers;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
        $this->setToken();
    }


    private function setToken()
    {
        $user = factory(App\User::class)->create();

        $data = [
            'email'          => $user->email,
            'password'      => '123456'
        ];
      
        
        $res = $this->post('/auth/login', $data)
                    ->response
                    ->getContent();

        return json_decode($res)->api_token;            
    }

    public function GetUrl($url)
    {
        $api_token = $this->setToken();
        $url .= '?token=' . $api_token;
        return $url;
    }
}
