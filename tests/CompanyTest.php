<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Libraries\Helpers;

class CompanyTest extends TestCase
{

    public function test_it_should_show_all_companies()
    {

       $url = $this->GetUrl(Helpers::apiPath('companies'));
       
       $this->get($url);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'data' => ['*' =>
                [
                    'name',
                    'parent_id',
                    'created_at',
                    'updated_at',
                    'childs'
                ]
            ]
        ]);
    }
}
