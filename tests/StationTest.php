<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Libraries\Helpers;

class StationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_it_should_return_all_stations()
    {
        $url = $this->GetUrl(Helpers::apiPath('stations'));

        $this->get($url);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'data' => ['*' =>
                [
                    'name',
                    'latitude',
                    'longitude',
                    'company',
                ]
            ]
        ]);

    }

    public function test_it_should_create_station()
    {
        $data = [
            'name'          => "Black",
            'latitude'      => '86.7765330',
            'longitude'     => '-75.7440000',
            'company_id'    => '2'
        ];

        $url = $this->GetUrl(Helpers::apiPath('stations'));
        $this->post($url, $data, []);

        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            [
                'name',
                'latitude',
                'longitude',
                'company_id',
                'created_at',
                'updated_at'
            ]
                
        );
    }

    public function test_it_should_delete_station()
    {

        $station = factory('App\Station')->create();
       
        $url = $this->GetUrl(Helpers::apiPath('stations/'.$station->id));

        $this->json('DELETE', $url);
        $this->seeStatusCode(200);
        $this->notSeeInDatabase('stations', ['id' => $station->id]);

    }

    public function test_it_should_update_station()
    {

        $station = factory('App\Station')->create();


        $data = [
            'name'          => "Edited",
            'latitude'      => '86.7765330',
            'longitude'     => '-75.7440000',
            'company_id'    => '1'
        ];

        $url = $this->GetUrl(Helpers::apiPath('stations/'.$station->id));
       
        $this->json('PUT', $url, $data, [] );
        $this->seeStatusCode(200);
        
        $this->seeInDatabase('stations', ['id' => $station->id, 'name' => "Edited", 'company_id' => 1]);

    }
}
