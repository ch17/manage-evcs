<?php

use Illuminate\Database\Seeder;

class StationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        \DB::statement("SET foreign_key_checks = 0");
        \App\Station::truncate();

        factory(App\Station::class, 30)->create();
    }
}
