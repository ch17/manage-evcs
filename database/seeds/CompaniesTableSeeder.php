<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement("SET foreign_key_checks = 0");
        \App\Company::truncate();
        \App\Company::create(['name' => 'Wayne Inc', 'parent_id' => 0]);
        \App\Company::create(['name' => 'Stark Crop', 'parent_id' => 0]);
        \App\Company::create(['name' => 'Gekko & Co', 'parent_id' => 1]);
        \App\Company::create(['name' => 'Wonka Industries', 'parent_id' => 1]);
        \App\Company::create(['name' => 'Hooli', 'parent_id' => 2]);
        \App\Company::create(['name' => 'Bubba Gump', 'parent_id' => 3]);
        \App\Company::create(['name' => 'Duff Beer', 'parent_id' => 6]); 
        \App\Company::create(['name' => 'Good Burger ', 'parent_id' => 6]); 
        \App\Company::create(['name' => 'Soylent Corp', 'parent_id' => 5]); 
        \App\Company::create(['name' => 'Adventureland', 'parent_id' => 1]); 
    }
}
