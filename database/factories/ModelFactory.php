<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => app('hash')->make('123456'),
    ];
});


$factory->define(App\Station::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->colorName,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
        'company_id' => $faker->numberBetween(1,10),
    ];
});
