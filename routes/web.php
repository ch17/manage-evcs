<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
   return $router->app->version();

 });

$router->post(
    'auth/login', 
    [
       'uses' => 'AuthController@authenticate'
    ]
);

$router->group(
    ['prefix' => 'api/v1', 'middleware' => 'jwt.auth'], 

    function() use ($router) {
        
        $router->get('/companies', ['uses' => 'CompanyController@index']);       
        $router->get('/companies/{id}', ['uses' => 'CompanyController@show']);       


        $router->get('/stations', ['uses' => 'StationController@index']);
        $router->post('/stations', ['uses' => 'StationController@create']);
        $router->delete('/stations/{id}', ['uses' => 'StationController@destroy']);
        $router->put('/stations/{id}', ['uses' => 'StationController@update']);

        $router->post('/search', 'SearchController@filter');
    }
);
