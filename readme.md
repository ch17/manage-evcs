# EVCS Management System for Devolon

A simple REST-API for the electric vehicle charging station management system. Including:

  - Company can be created with hierarchy
  - CURD operation for Stations
  - Each Station bellongs to a Company
  - Stations can be filtered by Company
  - Stations can be filtered by Latitude and Longitude
  - Test Cases for Stations CURD


### Tech

EVCS uses a number of open source projects to work properly:

* [Lumen](https://lumen.laravel.com/)- Micro-Framework by Laravel
* [JWT](https://jwt.io/introduction/) - JSON Web Token based authentication
* [Vue JS](https://vuejs.org/) - Javascricpt Framework for Frontend Demo


### Installation

EVCS requires [Composer](https://getcomposer.org/)  to run.

Clone repo and Install the dependencies and devDependencies and start the server.

```sh
$ git clone https://bitbucket.org/ch17/manage-evcs.git
```

Add ```DB_CREDENTIALS```,  ```APP_KEY ``` and ```JWT_SECRET``` to ```.env``` file 

```sh
$ composer update
$ php artisan migrate
$ php artisan db:seed
$ php -S localhost:8080 -t public
```

You are all set!

### Tests
```sh
$ vendor/bin/phpunit
```
